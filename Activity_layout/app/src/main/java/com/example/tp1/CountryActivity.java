package com.example.tp1;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Toast;


public class CountryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.country_activity);

        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("hashKey");
        String countryName = bundle.getString("countryName");
        final Country country = CountryList.getCountry(countryName);
        // Récupération de l'indice de l'image du pays
        Resources res = this.getResources();
        int imgId = res.getIdentifier(country.getmImgFile(), "drawable", this.getPackageName());

        TextView txtCountry         = (TextView) findViewById(R.id.txtCountry);
        ImageView imgFlagsCountry   = (ImageView) findViewById(R.id.imgFlagsCountry);
        final EditText txtBoxCapital      = (EditText) findViewById(R.id.txtBoxCapital);
        final EditText txtBoxTongue       = (EditText) findViewById(R.id.txtBoxTongue);
        final EditText txtBoxMoney        = (EditText) findViewById(R.id.txtBoxMoney);
        final EditText txtBoxPopulation   = (EditText) findViewById(R.id.txtBoxPopulation);
        final EditText txtBoxSurface      = (EditText) findViewById(R.id.txtBoxSurface);
        Button btnSave              = (Button) findViewById(R.id.btnSave);

        // Nom du pays
        txtCountry.setText(countryName);
        // Photo du drapeau
        imgFlagsCountry.setImageResource(imgId);
        // Pré-remplissage des textbox
        // Capitale
        txtBoxCapital.setText(country.getmCapital());
        // Langage
        txtBoxTongue.setText(country.getmLanguage());
        // Monnaie
        txtBoxMoney.setText(country.getmCurrency());
        // Population
        txtBoxPopulation.setText(Integer.toString(country.getmPopulation()));
        // Superficie
        txtBoxSurface.setText(Integer.toString(country.getmArea()));

        // Sauvegarde des modifications
        assert btnSave != null;
        btnSave.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int numberOfChanges = 0;

                // TODO: Modification des champs
                if (country.getmCapital().compareTo(txtBoxCapital.getText().toString()) != 0) {
                    numberOfChanges++;
                    country.setmCapital(txtBoxCapital.getText().toString());
                }
                if (country.getmLanguage().compareTo(txtBoxTongue.getText().toString()) != 0) {
                    numberOfChanges++;
                    country.setmLanguage(txtBoxTongue.getText().toString());
                }
                if (country.getmCurrency().compareTo(txtBoxMoney.getText().toString()) != 0) {
                    numberOfChanges++;
                    country.setmCurrency(txtBoxMoney.getText().toString());
                }
                if (country.getmPopulation() != Integer.parseInt(txtBoxPopulation.getText().toString())) {
                    numberOfChanges++;
                    country.setmPopulation(Integer.parseInt(txtBoxPopulation.getText().toString()));
                }
                if (country.getmArea() != Integer.parseInt(txtBoxSurface.getText().toString())) {
                    numberOfChanges++;
                    country.setmArea(Integer.parseInt(txtBoxSurface.getText().toString()));
                }

                // Affichage d'un toaster informant sur la sélection de l'utilisateur
                Toast.makeText (
                        getApplicationContext(),
                        "Number of changes saved: " + Integer.toString(numberOfChanges),
                        Toast.LENGTH_LONG
                ).show();

                // Dépile l'activité
                finish();
            }
        });


    }

}
