package com.example.tp1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.content.Intent;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ListView listView = (ListView) findViewById(R.id._countryList);

        // Créé une liste permettant de représenter le tableau
        // de String défini plus haut en attribut de la classe
        final ArrayAdapter adapter = new ArrayAdapter<String> (
                this,
                android.R.layout.simple_list_item_1,
                CountryList.getNameArray()
        );

        listView.setAdapter(adapter);

        // Définition d'une fonction gérant le clic sur un élément de
        // la liste générée précédemment
        listView.setOnItemClickListener ( new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick (AdapterView parent, View view, int position, long id) {
                // Action à réaliser en réaction au clic utilisateur
                final String item = (String) parent.getItemAtPosition(position);

                // Affichage d'un toaster informant sur la sélection de l'utilisateur
                Toast.makeText (
                        MainActivity.this,
                        "You selected item: " + item,
                        Toast.LENGTH_LONG
                ).show();

                // Déclaration d'une intention de changer d'activité
                Intent intent = new Intent(MainActivity.this, CountryActivity.class);

                // Passage de données entre activités
                Bundle bundle = new Bundle();
                bundle.putString("countryName", item);
                intent.putExtra("hashKey", bundle);

                startActivity(intent);
            }
        });
    }
}
