package com.example.data_persistence;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


public class CreateBookEntry extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        // Chargement de la base à partir du contexte de l'application
        final BookDbHelper bookDbHelper = new BookDbHelper(this);

        // Initialisation des champs
        final TextView nameBook         = (TextView) findViewById (R.id.nameBook);
        final TextView autheursBook     = (TextView) findViewById (R.id.editAuthors);
        final TextView yearBook         = (TextView) findViewById (R.id.editYear);
        final TextView genresBook       = (TextView) findViewById (R.id.editGenres);
        final TextView publisherBook    = (TextView) findViewById (R.id.editPublisher);

        Button saveButton = (Button) findViewById(R.id.button);
        assert saveButton != null;

        // Sauvegarde des modifications
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Book newBook = new Book (
                    nameBook.getText().toString(),
                    autheursBook.getText().toString(),
                    genresBook.getText().toString(),
                    publisherBook.getText().toString(),
                    yearBook.getText().toString()
                );

                bookDbHelper.addBook(newBook);

                Toast.makeText (
                        CreateBookEntry.this,
                        "New book saved!",
                        Toast.LENGTH_SHORT
                ).show();

                // Fin de l'activité et retour sur l'activite principale
                finish();
            }
        });
    }
}
