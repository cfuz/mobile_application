package com.example.data_persistence;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


public class BookActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        // Chargement de la base à partir du contexte de l'application
        final BookDbHelper bookDbHelper = new BookDbHelper(this);
        // Récupération des données à partir de l'Intent
        final Book currentBook = (Book) getIntent().getParcelableExtra("book_instance");
        final long id = (long) getIntent().getLongExtra("book_id", 0);

        // Initialisation des champs
        final TextView nameBook         = (TextView) findViewById (R.id.nameBook);
        final TextView autheursBook     = (TextView) findViewById (R.id.editAuthors);
        final TextView yearBook         = (TextView) findViewById (R.id.editYear);
        final TextView genresBook       = (TextView) findViewById (R.id.editGenres);
        final TextView publisherBook    = (TextView) findViewById (R.id.editPublisher);

        nameBook.setText(currentBook.getTitle());
        autheursBook.setText(currentBook.getAuthors());
        yearBook.setText(currentBook.getYear());
        genresBook.setText(currentBook.getGenres());
        publisherBook.setText(currentBook.getPublisher());

        Button saveButton = (Button) findViewById(R.id.button);
        assert saveButton != null;

        // Sauvegarde des modifications
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { // close view on click
                currentBook.setTitle(nameBook.getText().toString());
                currentBook.setAuthors(autheursBook.getText().toString());
                currentBook.setGenres(genresBook.getText().toString());
                currentBook.setPublisher(publisherBook.getText().toString());
                currentBook.setYear(yearBook.getText().toString());

                bookDbHelper.updateBook(currentBook);

                Toast.makeText (
                        BookActivity.this,
                        "Change(s) saved!",
                        Toast.LENGTH_SHORT
                ).show();

                // Fin de l'activité et retour sur l'activite principale
                finish();
            }
        });
    }
}
