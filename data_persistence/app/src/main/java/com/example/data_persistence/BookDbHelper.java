package com.example.data_persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class BookDbHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;
    private static final String TAG = BookDbHelper.class.getSimpleName();
    public static final String DATABASE_NAME = "book.db";
    public static final String TABLE_NAME = "library";
    public static final String _ID = "_id";
    public static final String COLUMN_BOOK_TITLE = "title";
    public static final String COLUMN_AUTHORS = "authors";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_GENRES = "genres";
    public static final String COLUMN_PUBLISHER = "publisher";


    public BookDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
	    db.execSQL (
            "CREATE TABLE " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY," +
                COLUMN_BOOK_TITLE + " TEXT," +
                COLUMN_AUTHORS + " TEXT," +
                COLUMN_YEAR + " INTEGER," +
                COLUMN_GENRES + " TEXT," +
                COLUMN_PUBLISHER + " TEXT" +
            ")"
        );
    }

    /**
     * MaJ de la table au changement
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);
    }


    /**
    * Adds a new book
    * @return  true if the book was added to the table ; false otherwise
    */
    public boolean addBook(Book book) {
        // Ouverture de la base
        SQLiteDatabase db       = this.getWritableDatabase();
        // Pour stockage des valeurs issues de l'objet book et traitement de la méthode insert()
        ContentValues values    = new ContentValues();

        // Inserting Row
        long rowID = 0;

        // Alimentation de la hashmap values
        values.put(COLUMN_BOOK_TITLE,   book.getTitle());
        values.put(COLUMN_AUTHORS,      book.getAuthors());
        values.put(COLUMN_PUBLISHER,    book.getPublisher());
        values.put(COLUMN_YEAR,         book.getYear());
        values.put(COLUMN_GENRES,       book.getGenres());

        // Insertion des données issues de l'objet book passé en paramètre dans la bdd
        // NOTA: insert() renvoie le numéro de la ligne qui a été changée
        rowID = db.insert(TABLE_NAME, null, values);

        // Fermeture de la base
        db.close();

        return (rowID != -1);
    }


    /**
     * Updates the information of a book inside the data base
     * @return the number of updated rows
     */
    public int updateBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();
        int res = 0;

        // MàJ de la table
        // Récupération de l'entree dans une variable temporaire
        ContentValues values = new ContentValues();
        values.put(_ID,                 book.getId());
        values.put(COLUMN_BOOK_TITLE,   book.getTitle());
        values.put(COLUMN_AUTHORS,      book.getAuthors());
        values.put(COLUMN_YEAR,         book.getYear());
        values.put(COLUMN_GENRES,       book.getGenres());
        values.put(COLUMN_PUBLISHER,    book.getPublisher());

        // Appel de la fonction de mise à jour (retourne le nombre de lignes mises a jour)
        res = db.update(TABLE_NAME, values, "_id=" + book.getId(), null);

        return res;
    }


    /**
     * Retourne toutes les entrées de la base
     *
     * @return  Un curseur contenant les informations de la ligne courante
     */
    public Cursor fetchAllBooks() {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        return cursor;
    }

    /**
     * Retoune toutes les entrées de la base correspondant à l'id entré en paramètre
     * @param bookId
     * @return
     */
    public Cursor fetchBookFromId(Long bookId) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query (
                TABLE_NAME,
                null,
                String.valueOf(bookId),
                null,
                null,
                null,
                null,
                null
        );

        if (cursor != null)
            cursor.moveToFirst();

        return cursor;
    }


    public void deleteBook(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        cursor.moveToFirst();

        String id = Long.toString(cursor.getLong(cursor.getColumnIndex("_id")));
        db.delete(TABLE_NAME,"_id = ?", new String[] {id});

        // call db.delete();
        db.close();
    }


    public void populate() {
        Log.d(TAG, "call populate()");

        addBook(new Book("Rouge Brésil", "J.-C. Rufin", "2003", "roman d'aventure, roman historique", "Gallimard"));
        addBook(new Book("Guerre et paix", "L. Tolstoï", "1865-1869", "roman historique", "Gallimard"));
        addBook(new Book("Fondation", "I. Asimov", "1957", "roman de science-fiction", "Hachette"));
        addBook(new Book("Du côté de chez Swan", "M. Proust", "1913", "roman", "Gallimard"));
        addBook(new Book("Le Comte de Monte-Cristo", "A. Dumas", "1844-1846", "roman-feuilleton", "Flammarion"));
        addBook(new Book("L'Iliade", "Homère", "8e siècle av. J.-C.", "roman classique", "L'École des loisirs"));
        addBook(new Book("Histoire de Babar, le petit éléphant", "J. de Brunhoff", "1931", "livre pour enfant", "Éditions du Jardin des modes"));
        addBook(new Book("Le Grand Pouvoir du Chninkel", "J. Van Hamme et G. Rosiński", "1988", "BD fantasy", "Casterman"));
        addBook(new Book("Astérix chez les Bretons", "R. Goscinny et A. Uderzo", "1967", "BD aventure", "Hachette"));
        addBook(new Book("Monster", "N. Urasawa", "1994-2001", "manga policier", "Kana Eds"));
        addBook(new Book("V pour Vendetta", "A. Moore et D. Lloyd", "1982-1990", "comics", "Hachette"));

        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);

        Log.d(TAG, "nb of rows="+numRows);

        // Fermeture de la base
        db.close();
    }


    // TODO
    public static Book cursorToBook(Cursor cursor) {
        cursor.moveToFirst();

        Book b = new Book (
                cursor.getLong(cursor.getColumnIndex("_id")),
                cursor.getString(cursor.getColumnIndex("title")),
                cursor.getString(cursor.getColumnIndex("authors")),
                cursor.getString(cursor.getColumnIndex("year")),
                cursor.getString(cursor.getColumnIndex("genres")),
                cursor.getString(cursor.getColumnIndex("publisher"))
        );

        return b;
    }
}
