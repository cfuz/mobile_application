package com.example.data_persistence;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.database.Cursor;



public class MainActivity extends AppCompatActivity {
    BookDbHelper bookDbHelper;
    Cursor cursor;
    SimpleCursorAdapter adapter;

    // Debugging
    private static final String TAG = MainActivity.class.getSimpleName(); // Récupère le nom de la classe

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Instanciation de la table des livres sous forme d'objets
        bookDbHelper = new BookDbHelper(this);
        // Ajout d'entrées dans la table nouvellement créée
        bookDbHelper.populate();
        // Creation d'un curseur de parcours de la table
        cursor = bookDbHelper.fetchAllBooks();

        // Création de la base de données
        // Get the data from the database
        adapter = new SimpleCursorAdapter (
                this,
                android.R.layout.simple_list_item_2,
                cursor,
                new String[]    { BookDbHelper.COLUMN_BOOK_TITLE, BookDbHelper.COLUMN_AUTHORS },
                new int[]       { android.R.id.text1, android.R.id.text2 }
        );

        final ListView listView = (ListView) findViewById(R.id.lstView);
        listView.setAdapter(adapter);

        listView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu (
                    ContextMenu menu,
                    View v,
                    ContextMenu.ContextMenuInfo menuInfo
            ) {
                MenuInflater mi = getMenuInflater();
                mi.inflate(R.menu.menu_activity_main, menu);
            }
        });


        // Definition d'une fonction gerant le clic sur un element de
        // la liste generee precedemment
        listView.setOnItemClickListener ( new AdapterView.OnItemClickListener() {
            // Action à réaliser en réaction au clic utilisateur
            @Override
            public void onItemClick (AdapterView parent, View view, int position, long id) {
                SQLiteDatabase tmpDb = bookDbHelper.getReadableDatabase();

                Cursor tmpCur = tmpDb.rawQuery (
                            "SELECT * FROM "+ BookDbHelper.TABLE_NAME
                                +" WHERE "+ BookDbHelper._ID +"="+ Long.toString(id),
                            null
                );

                tmpCur.moveToFirst();

                // Retourne le nom du livre selectionne
                Log.d (
                        TAG,
                        "Book selected: " + tmpCur.getString(1)
                                +"   Author: "+ tmpCur.getString(2)
                );

                // Stockage du resultat dans une instance de Book
                Book tmpBook = BookDbHelper.cursorToBook(tmpCur);

                // Envoi de l'intent a l'activite suivante
                Intent intent = new Intent(MainActivity.this, BookActivity.class);
                intent.putExtra("book_instance", tmpBook);
                intent.putExtra("book_id", id);
                startActivity(intent);
            }
        });

        // Bouton d'ajout de livre
        FloatingActionButton fab = findViewById(R.id.floatBtnAddBook);
        // Action a engager au clic
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CreateBookEntry.class);
                startActivity(intent);
            }
        });

    }

    // MaJ de la liste au retour sur la vue
    protected void onResume() {
        super.onResume();
        cursor = bookDbHelper.fetchAllBooks();
        adapter.swapCursor(cursor);
    }


    /**
     * Definition de l'action du menu contextuel au niveau de la liste des livres
     *
     * @param item
     * @return
     */
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.delete_menu) {
                AdapterView.AdapterContextMenuInfo data = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                SQLiteDatabase tmpDb = bookDbHelper.getReadableDatabase();

                // Selection de l'entite a supprimer dans la base
                Cursor tmpCur = tmpDb.rawQuery (
                        "SELECT * FROM " + BookDbHelper.TABLE_NAME
                                +" WHERE "+ BookDbHelper._ID +"="+ data.id,
                        null);

                // Appel de la fonction de suppression
                bookDbHelper.deleteBook(tmpCur);

                // MaJ de l'affichage apres suppression
                cursor = bookDbHelper.fetchAllBooks();
                adapter.swapCursor(cursor);
        }
        return true;
    }


    /**
     * Prise en charge des clics sur des elements de la liste, pour affichage de la barre de menu.
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }
}





/*
// Affichage d'un toaster informant sur la sélection de l'utilisateur
Toast.makeText (
        MainActivity.this,
        "You selected item: " + id,
        Toast.LENGTH_LONG
).show();

// Inflate exemple
@Override
public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_activity_main, menu);
    return true;
}
*/