package com.example.weatherapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class WeatherDbHelper extends SQLiteOpenHelper {
    // Attributes
    private static final String TAG = WeatherDbHelper.class.getSimpleName();
    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "weather.db";
    public static final String TABLE_NAME = "weather";
    public static final String _ID = "_id";
    public static final String COLUMN_CITY_NAME = "city";
    public static final String COLUMN_COUNTRY = "country";
    public static final String COLUMN_TEMPERATURE = "temperature";
    public static final String COLUMN_HUMIDITY = "humidity";
    public static final String COLUMN_WIND_SPEED = "windSpeed";
    public static final String COLUMN_WIND_DIRECTION = "windDirection";
    public static final String COLUMN_CLOUDINESS = "cloudiness";
    public static final String COLUMN_ICON = "icon";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_LAST_UPDATE = "lastupdate";
    public static final String COLUMN_SUNSET = "sunset";

    public WeatherDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_BOOK_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
            _ID + " INTEGER PRIMARY KEY," +
            COLUMN_CITY_NAME + " TEXT NOT NULL, " +
            COLUMN_COUNTRY + " TEXT NOT NULL, " +
            COLUMN_TEMPERATURE + " INTEGER, " +
            COLUMN_HUMIDITY+ " TEXT, " +
            COLUMN_WIND_SPEED+ " TEXT, " +
            COLUMN_WIND_DIRECTION+ " TEXT, " +
            COLUMN_CLOUDINESS+ " TEXT, " +
            COLUMN_DESCRIPTION+ " TEXT, " +
            COLUMN_ICON+ " TEXT, " +
            COLUMN_LAST_UPDATE+ " TEXT, " +
            // To assure the application have just one weather entry per
            // city name and country, it's created a UNIQUE
            " UNIQUE (" + COLUMN_CITY_NAME + ", " + COLUMN_COUNTRY + ") " +
            "ON CONFLICT ROLLBACK" +
        ");";
        db.execSQL(SQL_CREATE_BOOK_TABLE);
    }

    /**
     * Standard onUpgrade() function
     * @author  JHE
     * @date    28.02.19
     * @param   db
     * @param   oldVersion
     * @param   newVersion
     */
    @Override public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    /**
     * Resets the table
     */
    public void resetDB() {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.delete(TABLE_NAME, null, null);
        } catch (Exception e) {
            Log.d(TAG, "getAllCities(): [ERROR:"+ e.getMessage() +"] While requesting database ...");
        }
    }

    /**
     * Adds a new city
     * @return  true if the city was added to the table ; false otherwise (case when the pair
     *          (city name, country) is already in the data base
     */
    public boolean addCity(City city) {
        SQLiteDatabase db = this.getWritableDatabase();
        // Defining new entry to put
        ContentValues values = new ContentValues();
        values.put(COLUMN_CITY_NAME, city.getName());
        values.put(COLUMN_COUNTRY, city.getCountry());
        values.put(COLUMN_TEMPERATURE, city.getTemperature());
        values.put(COLUMN_HUMIDITY, city.getHumidity());
        values.put(COLUMN_WIND_SPEED, city.getWindSpeed());
        values.put(COLUMN_WIND_DIRECTION, city.getWindDirection());
        values.put(COLUMN_CLOUDINESS, city.getCloudiness());
        values.put(COLUMN_ICON, city.getIcon());
        values.put(COLUMN_DESCRIPTION, city.getDescription());
        values.put(COLUMN_LAST_UPDATE, city.getLastUpdate());
        // Debugging message
        Log.d(TAG, "adding: "+city.getName()+" with id="+city.getId());
        // Inserting Row
        // The unique used for creating table ensures to have only one copy of each pair (city name, country)
        // If rowID = -1, an error occured
        long rowID = db.insertWithOnConflict(TABLE_NAME, null, values, CONFLICT_IGNORE);
        // Closing database connection
        db.close();
        return (rowID != -1);
    }

    /**
     * Updates the information of a city inside the data base
     * @return the number of updated rows
     */
    public int updateCity(City city) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_CITY_NAME, city.getName());
        values.put(COLUMN_COUNTRY, city.getCountry());
        values.put(COLUMN_TEMPERATURE, city.getTemperature());
        values.put(COLUMN_HUMIDITY, city.getHumidity());
        values.put(COLUMN_WIND_SPEED, city.getWindSpeed());
        values.put(COLUMN_WIND_DIRECTION, city.getWindDirection());
        values.put(COLUMN_CLOUDINESS, city.getCloudiness());
        values.put(COLUMN_ICON, city.getIcon());
        values.put(COLUMN_DESCRIPTION, city.getDescription());
        values.put(COLUMN_LAST_UPDATE, city.getLastUpdate());
        // Updating row
        return db.updateWithOnConflict (TABLE_NAME, values, _ID + " = ?", new String[] { String.valueOf(city.getId()) }, CONFLICT_IGNORE);
    }

    /**
     * @return  A cursor on all the cities of the data base
     * */
    public Cursor fetchAllCities() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query (
            TABLE_NAME, null, null, null, null, null,
            COLUMN_CITY_NAME+" ASC", null
        );
        Log.d(TAG, "call fetchAllCities()");
        if (cursor != null)
            cursor.moveToFirst();
        return cursor;
    }

    // TODO: functionnality test
    /**
     * Returns a list on all the cities of the data base
     * @author  JHE
     * @date    28/02/2019
     * @return  List of all the cities of the db
     * */
    public List<City> getAllCities() {
        SQLiteDatabase sqlDB = getReadableDatabase();
        Cursor tmpCursor = null;
        List<City> lstCity = null;
        try {
            Log.d(TAG, "--INFO-- getAllCities(): cursor instanciation ...");
            tmpCursor = sqlDB.rawQuery("SELECT *  FROM "+ TABLE_NAME, null);
        } catch (Exception e) {
            Log.d(TAG, "--ERROR-- getAllCities(): ["+ e.getMessage() +"]While requesting database ...");
        }
        try {
            if (tmpCursor.moveToFirst()) {
                lstCity = new ArrayList<>();
                do {
                    Log.d(TAG, "--INFO-- getAllCities(): Adding new city to list ...");
                    lstCity.add (
                        new City(
                            tmpCursor.getLong(tmpCursor.getColumnIndex(_ID)),
                            tmpCursor.getString(tmpCursor.getColumnIndex(COLUMN_CITY_NAME)),
                            tmpCursor.getString(tmpCursor.getColumnIndex(COLUMN_COUNTRY)),
                            tmpCursor.getString(tmpCursor.getColumnIndex(COLUMN_TEMPERATURE)),
                            tmpCursor.getString(tmpCursor.getColumnIndex(COLUMN_HUMIDITY)),
                            tmpCursor.getString(tmpCursor.getColumnIndex(COLUMN_WIND_SPEED)),
                            tmpCursor.getString(tmpCursor.getColumnIndex(COLUMN_WIND_DIRECTION)),
                            tmpCursor.getString(tmpCursor.getColumnIndex(COLUMN_CLOUDINESS)),
                            tmpCursor.getString(tmpCursor.getColumnIndex(COLUMN_ICON)),
                            tmpCursor.getString(tmpCursor.getColumnIndex(COLUMN_DESCRIPTION)),
                            tmpCursor.getString(tmpCursor.getColumnIndex(COLUMN_LAST_UPDATE))
                        )
                    );
                } while (tmpCursor.moveToNext());
            } else {
                Log.d(TAG, "--WARN.-- No input in "+ TABLE_NAME +" found ...");
            }
        } catch(Exception e) {
            Log.d(TAG, "--ERROR-- getAllCities(): ["+ e.getMessage() +"] While instanciating City list ...");
        }
        if (lstCity == null)
            Log.d(TAG, "--WARN.-- No city list returned ...");
        return lstCity;
    }

    /**
     * Deletes an entry from the database
     * @param   cursor  The current entry to delete under the form of a cursor
     */
    public void deleteCity(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete (TABLE_NAME, _ID + " = ?", new String[] { cursor.getString(cursor.getColumnIndex(_ID)) });
        db.close();
    }

    /**
     * Deletes an entry from its id in the table
     * @param   id
     */
    public void deleteCity(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, _ID + " = ?", new String[]{ String.valueOf(id) });
        db.close();
    }

    public void populate() {
        Log.d(TAG, "call populate()");
        addCity(new City("Avignon","FR"));
        addCity(new City("Paris","FR"));
        addCity(new City("Rennes","FR"));
        addCity(new City("Montreal","CA"));
        addCity(new City("Fortaleza","BR"));
        addCity(new City("Papeete","PF"));
        addCity(new City("Sydney","AU"));
        addCity(new City("Seoul","KR"));
        addCity(new City("Bamako","ML"));
        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }

    /**
     * Transform current cursor content into City instance
     * @param   cursor
     * @return  City object
     */
    public City cursorToCity(Cursor cursor) {
        City city = new City(
            cursor.getLong(cursor.getColumnIndex(_ID)),
            cursor.getString(cursor.getColumnIndex(COLUMN_CITY_NAME)),
            cursor.getString(cursor.getColumnIndex(COLUMN_COUNTRY)),
            cursor.getString(cursor.getColumnIndex(COLUMN_TEMPERATURE)),
            cursor.getString(cursor.getColumnIndex(COLUMN_HUMIDITY)),
            cursor.getString(cursor.getColumnIndex(COLUMN_WIND_SPEED)),
            cursor.getString(cursor.getColumnIndex(COLUMN_WIND_DIRECTION)),
            cursor.getString(cursor.getColumnIndex(COLUMN_CLOUDINESS)),
            cursor.getString(cursor.getColumnIndex(COLUMN_ICON)),
            cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)),
            cursor.getString(cursor.getColumnIndex(COLUMN_LAST_UPDATE))
        );
        return city;
    }

    // TODO: Functionnality test
    /**
     * Returns a City instance by its id
     * @author  JHE
     * @date    28.02.2018
     * @param   id
     * @return  City instance by its id
     */
    public City getCity(long id) {
        // Opening table in reading mode
        SQLiteDatabase sqlDB = getReadableDatabase();
        City city = null;
        Cursor tmpCursor = null;
        // Requesting database
        try {
            tmpCursor = sqlDB.rawQuery(
                "SELECT * FROM " + TABLE_NAME + " WHERE " + _ID + "=?",
                new String[] {Long.toString(id)}
            );
        } catch (Exception e) {
            Log.d(TAG, "--ERROR-- getCity():["+ e.getMessage() +"] While accessing "+ TABLE_NAME +" database ...");
        }
        // City instanciation
        try {
            if (tmpCursor != null) {
                tmpCursor.moveToFirst();
                city = this.cursorToCity(tmpCursor);
            } else {
                Log.d(TAG, "--INFO-- getCity(): No city list found with "+ Long.toString(id) +" ...");
            }
        } catch (Exception e) {
            Log.d(TAG, "--ERROR-- getCity(): ["+ e.getMessage() +"] While instanciating City object ...");
        }
        if (city == null)
            Log.d(TAG, "--WARN.-- getCity(): No city returned ...");
        return city;
    }
}
