package com.example.weatherapp;

import android.util.JsonReader;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.Date;

/**
 * Process the response to a GET request to the Web service: api.openweathermap.org
 * Responses must be provided in JSON.
 * */
public class JSONResponseHandler {
    private static final String TAG = JSONResponseHandler.class.getSimpleName();
    private City city;

    public JSONResponseHandler(City city) {
        this.city = city;
    }

    /**
     * @param   response done by the Web service
     * @return  A City with attributes filled with the collected information if response was
     *          successfully analyzed; a void list otherwise
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readCity(reader);
        } finally {
            reader.close();
        }
    }

    public City getCity() { return this.city; }

    private void readCity(JsonReader reader) throws IOException {
        reader.beginObject();
        String name;
        while (reader.hasNext()) {
            name = reader.nextName();
            switch (name) {
                case "weather": // Description and icon
                    readWeather(reader);
                    break;
                case "main": // Temperature and humidity
                    readMain(reader);
                    break;
                case "wind": // Wind speed and wind direction
                    readWind(reader);
                    break;
                case "clouds": // Cloudiness and
                    readClouds(reader);
                    break;
                case "dt":
                    city.setLastUpdate(unixTime2date(reader.nextLong()));
                    break;
                case "sys":
                    readSys(reader);
                    break;
                case "name":
                    city.setName(reader.nextString());
                    break;
                default:
                    reader.skipValue();
                    break;
            }
        }
        reader.endObject();
    }

    private void readWeather(JsonReader reader) throws IOException {
        reader.beginArray();
        String name;
        if (reader.hasNext()) {
            // TODO
            reader.beginObject();
            while (reader.hasNext()) {
                name = reader.nextName();
                switch (name) {
                    case "description":
                        city.setDescription(reader.nextString());
                        break;
                    case "icon":
                        city.setIcon(reader.nextString());
                        break;
                    default:
                        reader.skipValue();
                        break;
                }
            }
            reader.endObject();
        }
        reader.endArray();
    }

    private void readMain(JsonReader reader) throws IOException {
        reader.beginObject();
        String name;
        while (reader.hasNext()) {
            name = reader.nextName();
            switch (name) {
                case "temp":
                    city.setTemperature(kelvin2celsius(reader.nextDouble()));
                    break;
                case "humidity":
                    // TODO
                    city.setHumidity(new DecimalFormat("##0.##").format(
                            reader.nextDouble()
                    ));
                    break;
                default:
                    reader.skipValue();
                    break;
            }
        }
        reader.endObject();
    }

    private void readWind(JsonReader reader) throws IOException {
        reader.beginObject();
        String name;
        while (reader.hasNext()) {
            name = reader.nextName();
            switch (name) {
                case "speed":
                    // TODO
                    city.setWindSpeed(ms2kmh(reader.nextDouble()));
                    break;
                case "deg":
                    city.setWindDirection(deg2compass(reader.nextInt()));
                    break;
                default:
                    reader.skipValue();
                    break;
            }
        }
        reader.endObject();
    }

    private void readClouds(JsonReader reader) throws IOException {
        reader.beginObject();
        // TODO
        while (reader.hasNext()) {
            if (reader.nextName().equals("all"))
                city.setCloudiness(new DecimalFormat("##0.##").format(
                        reader.nextDouble()
                )+ "%");
            else
                reader.skipValue();
        }
        reader.endObject();
    }

    public void readSys(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            if (reader.nextName().equals("country"))
                city.setCountry(reader.nextString());
            else
                reader.skipValue();
        }
        reader.endObject();
    }

    private String unixTime2date(long time) {
        Date date = new Date(time*1000);
        return date.toString();
    }

    private String kelvin2celsius(double t) {
        Log.d(TAG, "read temperature="+ t);
        return String.valueOf((int)(t-273.15));
    }

    private String farenheit2celsius(double t) {
        return String.valueOf((int) ((5.0/9.0) * (t-32)));
    }

    private String mph2kmh(double speed) {
        return String.valueOf((int) (speed*1.609344));
    }

    private String ms2kmh(double speed) {
        return (String.valueOf(new DecimalFormat("###0.##").format(
                speed*(3.60))
        ));
    }

    private String deg2compass(int deg) {
        String[] arrComp = {"N","NNE","NE","ENE","E","ESE", "SE", "SSE","S","SSW","SW","WSW","W","WNW","NW","NNW"};
        int val = (int) ((((float) deg) / 22.5) + .5);
        return arrComp[val % 16];
    }
}
