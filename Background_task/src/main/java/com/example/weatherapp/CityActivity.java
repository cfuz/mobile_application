package com.example.weatherapp;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class CityActivity extends AppCompatActivity {
    public static final String EXTRA_CITY = "com.example.weatherapp.EXTRA_CITY";
    private static final String TAG = CityActivity.class.getSimpleName();
    private TextView textCityName, textCountry, textTemperature, textHumdity, textWind, textCloudiness, textLastUpdate;
    private Button btnUpdate;
    private ImageView imageWeatherCondition;
    private City city;
    private boolean updated = false;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);
        // Retrieve city object from intent
        city = (City) getIntent().getParcelableExtra(City.TAG);
        // Pre-filling fields of the city activity form
        textCityName = (TextView) findViewById(R.id.cityName);
        textCountry = (TextView) findViewById(R.id.countryName);
        textTemperature = (TextView) findViewById(R.id.editTemperature);
        textHumdity = (TextView) findViewById(R.id.editHumidity);
        textWind = (TextView) findViewById(R.id.editWind);
        textCloudiness = (TextView) findViewById(R.id.editCloudiness);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);
        // Get the image corresponding to the current weather
        imageWeatherCondition = (ImageView) findViewById(R.id.imageView);
        // Udpdating information about city's weather
        updateView();
        // Update button initialization and event definition
        btnUpdate = (Button) findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                // TODO
                try {
                    new AsyncTaskCityUpdater(city).execute (
                            WebServiceUrl.build(city.getName(), city.getCountry())
                    );
                } catch (MalformedURLException e) {
                    Log.d(TAG, "btnUpdate.onClick(): "+ e.getMessage());
                } catch (UnsupportedEncodingException e) {
                    Log.d(TAG, "btnUpdate.onClick(): "+ e.getMessage());
                }
                updateView();
            }
        });
    }

    @Override public void onBackPressed() {
        //TODO : prepare result for the main activity
        Intent i = new Intent(CityActivity.this, MainActivity.class);
        if (updated == true) {
            i.putExtra(EXTRA_CITY, this.city);
            setResult(Activity.RESULT_OK, i);
        } else {
            setResult(Activity.RESULT_CANCELED, i);
        }
        super.onBackPressed();
    }

    private void updateView() {
        // Updating text-boxes
        textCityName.setText(city.getName());
        textCountry.setText(city.getCountry());
        textTemperature.setText(city.getTemperature()+" °C");
        textHumdity.setText(city.getHumidity()+" %");
        textWind.setText(city.getFullWind());
        textCloudiness.setText(city.getCloudiness());
        textLastUpdate.setText(city.getLastUpdate());
        // If an icon is defined in the current city instance, we define it in the image beacon
        if (city.getIcon()!=null && !city.getIcon().isEmpty()) {
            Log.d(TAG,"icon: icon_" + city.getIcon());
            imageWeatherCondition.setImageDrawable (
                getResources().getDrawable (
                    getResources().getIdentifier(
                        "@drawable/icon_" + city.getIcon(),
                        null,
                        getPackageName()
                    )
                )
            );
            imageWeatherCondition.setContentDescription(city.getDescription());
        }
    }

    /**
     * @source  https://stackoverflow.com/questions/34916781/calling-web-api-and-receive-return-value-in-android
     *          https://abhiandroid.com/programming/asynctask
     *          https://www.androidauthority.com/use-remote-web-api-within-android-app-617869/
     *          https://stackoverflow.com/questions/9671546/asynctask-android-example
     */
    public class AsyncTaskCityUpdater extends AsyncTask<URL, Integer, City> {
        private final String TAG = AsyncTaskCityUpdater.class.getSimpleName();
        private City cityToUpdate;

        public AsyncTaskCityUpdater(City city) { this.cityToUpdate = city; }

        /**
         * HttpURLConnection method to get JSON data from Webservice and retrieve under the form of a
         * City instance.
         * @param   updateURL
         * @return  Result from request with JSON generated string
         */
        @Override protected City doInBackground(URL... updateURL) {
            JSONResponseHandler jsonRH = new JSONResponseHandler(this.cityToUpdate);
            // Code that will run in the background
            try {
                // Openning connection and connection settings
                HttpURLConnection connection = (HttpURLConnection) updateURL[0].openConnection();
                connection.setRequestMethod("GET");
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.connect();
                if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    Log.d(TAG, "getCityFromwebServiceCall(): Connection successful!");
                    try {
                        Log.d(TAG, "getCityFromwebServiceCall(): Reading JSON response from given URL ["+ updateURL[0] +"]");
                        InputStream in = new BufferedInputStream(connection.getInputStream());
                        jsonRH.readJsonStream(in); // Updating city from JSON string result
                    } catch (Exception e) {
                        Log.d(TAG, "doInBackground(): ERROR while reading input from API...");
                        e.printStackTrace();
                        return  null;
                    }
                    return jsonRH.getCity();
                } else {
                    Log.d(TAG, "doInBackground(): ERROR Connection failed...");
                    return null;
                }
            } catch (Exception e) {
                Log.d(TAG, "doInBackground(): ERROR while trying to connect to URL ["+ updateURL[0] +"]");
                return null;
            }
        }

        @Override protected void onPostExecute(City updatedCity) {
            Log.d(TAG, "onPostExecute(): retrieving result");
            updated = true; // City has been updated we can then use it to update the db further in the app
            updateView(); // Sets the form's fields up to date
        }
    }
}
