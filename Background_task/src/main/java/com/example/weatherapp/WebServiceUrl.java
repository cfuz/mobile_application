package com.example.weatherapp;

import android.net.Uri;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class WebServiceUrl {
    // TODO: complete with your own API_KEY
    private static final String API_KEY = "541c863b3b4df826cf49545ba15b5358";
    //private static final String HOST = "samples.openweathermap.org";
    private static final String HOST = "api.openweathermap.org";
    private static final String PATH_1 = "data";
    private static final String PATH_2 = "2.5";
    private static final String PATH_3 = "weather";
    private static final String URL_PARAM1 = "q";
    private static final String URL_PARAM2 = "appid";

    public static URL build(String cityName, String countryName) throws UnsupportedEncodingException, MalformedURLException {
        Uri.Builder builder = new Uri.Builder();
        URL url;
        builder.scheme("https")
                .authority(HOST)
                .appendPath(PATH_1)
                .appendPath(PATH_2)
                .appendPath(PATH_3)
                .appendQueryParameter(URL_PARAM1, cityName + "," + countryName)
                .appendQueryParameter(URL_PARAM2, API_KEY);
        System.out.println(builder.build().toString());
        System.out.println(URLDecoder.decode(builder.build().toString(), "UTF-8"));
        return new URL(URLDecoder.decode(builder.build().toString(), "UTF-8"));
    }
}
