package com.example.weatherapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.weatherapp.City;


public class NewCityActivity extends AppCompatActivity {
    // For extra content
    public static final String EXTRA_NEW_CITY = "com.example.weatherapp.EXTRA_NEW_CITY";
    public static final String EXTRA_NEW_COUNTRY = "com.example.weatherapp.EXTRA_NEW_COUNTRY";
    // Forms of the current activity
    private EditText textName, textCountry;
    private Button btnAddNewCity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_city);
        textName = (EditText) findViewById(R.id.editNewName);
        textCountry = (EditText) findViewById(R.id.editNewCountry);
        btnAddNewCity = (Button) findViewById(R.id.btnAddNewCity);
        btnAddNewCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(NewCityActivity.this, MainActivity.class);
                final String newCityName = textName.getText().toString();
                final String newCountryName = textCountry.getText().toString();
                if (newCityName == null || newCountryName == null || newCityName.equals("") || newCountryName.equals("")) {
                    Toast.makeText (
                        NewCityActivity.this,
                        "No field(s) provided! Back to main activity ...",
                        Toast.LENGTH_SHORT
                    ).show();
                    setResult(Activity.RESULT_CANCELED, i);
                } else {
                    i.putExtra(EXTRA_NEW_CITY, newCityName);
                    i.putExtra(EXTRA_NEW_COUNTRY, newCountryName);
                    setResult(Activity.RESULT_OK, i);
                }
                // End the current activity and back to the main activity
                finish();
            }
        });
    }


}
