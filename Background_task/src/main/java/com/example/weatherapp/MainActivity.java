package com.example.weatherapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = MainActivity.class.getSimpleName();
    static final int NEW_CITY_ACTIVITY_REQUEST = 1;
    static final int CITY_ACTIVITY_REQUEST = 2;
    private WeatherDbHelper wdbh;
    SimpleCursorAdapter ca;
    private ListView lv;
    private Button btnAddCity;
    SwipeRefreshLayout srl;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // DB initialization
        wdbh = new WeatherDbHelper(this);
        wdbh.resetDB();
        wdbh.populate();
        // Creating a cursor that contains all the entries from the city db
        ca = new SimpleCursorAdapter (
            this,
            android.R.layout.simple_list_item_2,
            wdbh.fetchAllCities () ,
            new String[] { WeatherDbHelper.COLUMN_CITY_NAME, WeatherDbHelper.COLUMN_COUNTRY },
            new int[] { android.R.id.text1, android.R.id.text2}
        );
        // City listView hydration
        lv = (ListView) findViewById(R.id.lstCity);
        lv.setAdapter(ca);
        // onClick() event handler
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openCityActivity(id);
            }
        });
        // Button definition and initialization
        btnAddCity = (Button) findViewById(R.id.btnAddCity);
        btnAddCity.setOnClickListener(this);
        // Loading bar with update event handler
        srl = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        srl.setOnRefreshListener(this);
        srl.setColorScheme (
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    @Override public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAddCity:
                Toast.makeText(MainActivity.this, "Button btnAddCity clicked!", Toast.LENGTH_SHORT);
                this.openNewCityActivity();
                break;
        }
    }

    public void openNewCityActivity() {
        // this:context, NewCityActivity.class:name of the next activity
        Intent i = new Intent(this, NewCityActivity.class);
        startActivityForResult(i, NEW_CITY_ACTIVITY_REQUEST);
    }

    public void openCityActivity(long id) {
        final City cityInstance = wdbh.getCity(id);
        Toast.makeText (
                MainActivity.this,
                "You selected id#"+ Long.toString(id) +": "+ cityInstance.toString(), Toast.LENGTH_SHORT
        ).show();
        Log.d(TAG,"--INFO-- openActivity(): "+ cityInstance.getName());
        Intent i = new Intent(this, CityActivity.class);
        i.putExtra(City.TAG, cityInstance);
        startActivityForResult(i, CITY_ACTIVITY_REQUEST);
    }

    @Override public void onActivityResult(int requestCode, int resultCode, Intent resultIntent) {
        // Check which request we're responding to
        switch (requestCode) {
            case NEW_CITY_ACTIVITY_REQUEST:
                // Make sure the request was successful
                if (resultCode == RESULT_OK) {
                    // The user actually entered a new city, we can now update the database from the
                    // data, retrieved by the action previously called.
                    wdbh.addCity(new City(
                            resultIntent.getStringExtra(NewCityActivity.EXTRA_NEW_CITY),
                            resultIntent.getStringExtra(NewCityActivity.EXTRA_NEW_COUNTRY)
                    ));
                    // Updating cursor data to further display in list
                    ca.changeCursor(wdbh.fetchAllCities());
                    // Refresh listView
                    ca.notifyDataSetChanged();
                    Toast.makeText(
                            MainActivity.this,
                            "Content refreshed!",
                            Toast.LENGTH_SHORT
                    ).show();
                } else {
                    Toast.makeText (
                            MainActivity.this,
                            "No data to process ...",
                            Toast.LENGTH_SHORT
                    ).show();
                }
                break;
            case CITY_ACTIVITY_REQUEST:
                // Make sure the request was successful
                if (resultCode == RESULT_OK) {
                    // The user actually updated the city he selected
                    City c = (City) resultIntent.getParcelableExtra(CityActivity.EXTRA_CITY);
                    wdbh.updateCity(c);
                    // Updating cursor data to further display in list
                    ca.changeCursor(wdbh.fetchAllCities());
                    // Refresh listView
                    ca.notifyDataSetChanged();
                    Toast.makeText(
                            MainActivity.this,
                            "City "+ c.getFullName() +" updated!",
                            Toast.LENGTH_SHORT
                    ).show();
                } else {
                    Toast.makeText (
                            MainActivity.this,
                            "No update to process ...",
                            Toast.LENGTH_SHORT
                    ).show();
                }
                break;
        }
    }

    @Override public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                srl.setRefreshing(false);
                List<City> lstCity = wdbh.getAllCities();
                AsyncTaskListUpdater atlu = new AsyncTaskListUpdater(lstCity);
                atlu.execute();
            }
        }, 1000);
    }

    /**
     * @source  https://stackoverflow.com/questions/34916781/calling-web-api-and-receive-return-value-in-android
     *          https://abhiandroid.com/programming/asynctask
     *          https://www.androidauthority.com/use-remote-web-api-within-android-app-617869/
     *          https://stackoverflow.com/questions/9671546/asynctask-android-example
     */
    private class AsyncTaskListUpdater extends AsyncTask<Void, Integer, List<City>> {
        private final String TAG = MainActivity.AsyncTaskListUpdater.class.getSimpleName();
        private List<City> lstCities;

        public AsyncTaskListUpdater(List<City> lstCities) { this.lstCities = lstCities; }

        /**
         * HttpURLConnection method to get JSON data from Webservice and retrieve under the form of a
         * City instance.
         * @param   urls
         * @return  Result from request with JSON generated string
         */
        @Override protected List<City> doInBackground(Void... urls) {
            URL updateURL;
            for (City cityToUpdate: this.lstCities) {
                // Code that will run in the background
                try {
                    JSONResponseHandler jsonRH = new JSONResponseHandler(cityToUpdate);
                    updateURL = WebServiceUrl.build(cityToUpdate.getName(), cityToUpdate.getCountry());
                    // Openning connection and connection settings
                    HttpURLConnection connection = (HttpURLConnection) updateURL.openConnection();
                    connection.setRequestMethod("GET");
                    connection.setDoInput(true);
                    connection.setDoOutput(true);
                    connection.connect();
                    if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        Log.d(TAG, "getCityFromwebServiceCall(): Connection successful!");
                        try {
                            Log.d(TAG, "getCityFromwebServiceCall(): Reading JSON response from given URL [" + updateURL + "]");
                            InputStream in = new BufferedInputStream(connection.getInputStream());
                            jsonRH.readJsonStream(in); // Updating city from JSON string result
                        } catch (Exception e) {
                            Log.d(TAG, "doInBackground(): ERROR while reading input from API...");
                            e.printStackTrace();
                            return null;
                        }
                        cityToUpdate = jsonRH.getCity();
                    } else {
                        Log.d(TAG, "doInBackground(): ERROR Connection failed...");
                        return null;
                    }
                } catch (MalformedURLException e) {
                    Log.d(TAG, "btnUpdate.onClick(): " + e.getMessage());
                    return null;
                } catch (UnsupportedEncodingException e) {
                    Log.d(TAG, "btnUpdate.onClick(): " + e.getMessage());
                    return null;
                } catch (Exception e) {
                    Log.d(TAG, "doInBackground(): ERROR while trying to connect to URL");
                    return null;
                }
            }
            return lstCities;
        }

        @Override protected void onPostExecute(List<City> updatedCities) {
            Log.d(TAG, "onPostExecute(): retrieving result");
            for (City updatedCity : this.lstCities) {
                wdbh.updateCity(updatedCity); // Sets the current city up to date in db
                // Updating cursor data to further display in list
                ca.changeCursor(wdbh.fetchAllCities());
                // Refresh listView
                ca.notifyDataSetChanged();
            }
            Toast.makeText(
                    MainActivity.this,
                    "List refreshed!",
                    Toast.LENGTH_SHORT
            ).show();
        }
    }
}
